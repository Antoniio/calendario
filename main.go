// Package main
package main

// This is the branch on master

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"io/ioutil"
)

func  check(e error) {
	if e != nil {
		panic(e)
	}	
}


func main() {
	fmt.Println("El método main")
	
	r := gin.Default()
	
	r.GET("/ping",
		func(c *gin.Context) {
			c.JSON(200, gin.H{"message": "PONG"})
		})

	r.GET("calendario/:file",
		func(c *gin.Context) {
			file := c.Param("file")
			dat, err := ioutil.ReadFile(file)
			check(err)
			fmt.Print(string(dat))
			c.String(200, fmt.Sprint(string(dat)))
		})
	
	r.Run()
	
}

